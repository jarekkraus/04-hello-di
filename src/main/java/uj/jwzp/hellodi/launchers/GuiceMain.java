package uj.jwzp.hellodi.launchers;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import uj.jwzp.hellodi.logic.savers.*;
import uj.jwzp.hellodi.model.Movie;
import uj.jwzp.hellodi.logic.CSVMovieFinder;
import uj.jwzp.hellodi.logic.MovieFinder;
import uj.jwzp.hellodi.logic.MovieLister;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class GuiceMain {

    public static void main(String[] args) throws IOException {

        FileName file = new FileName(args);
        String fileName = file.getFileName();
        String fileFormat = file.getFileFormat();

        Injector injector = Guice.createInjector(new GuiceMovieModule());

        FileName fileBind = injector.getInstance(FileName.class);

        fileBind.setFileName(fileName);
        fileBind.setFileFormat(fileFormat);

        MovieLister lister = injector.getInstance(MovieLister.class);

        MovieSaver saver = injector.getInstance(Key.get(MovieSaver.class, Names.named(fileFormat)));

        List<Movie> movies = lister.moviesDirectedBy("Lucas").stream()
            .peek(m -> System.out.println(m.toString()))
            .collect(Collectors.toList());
        saver.save(movies);
    }
}

class GuiceMovieModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(MovieFinder.class).to(CSVMovieFinder.class);
        bind(MovieSaver.class).annotatedWith(Names.named("xml")).to(XmlMovieSaver.class);
        bind(MovieSaver.class).annotatedWith(Names.named("json")).to(JsonMovieSaver.class);
        bind(MovieSaver.class).annotatedWith(Names.named("yml")).to(YmlMovieSaver.class);
    }
}
