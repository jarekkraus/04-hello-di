package uj.jwzp.hellodi.launchers;

import com.google.inject.internal.cglib.proxy.$Dispatcher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import uj.jwzp.hellodi.logic.savers.FileName;
import uj.jwzp.hellodi.logic.savers.MovieSaver;
import uj.jwzp.hellodi.model.Movie;
import uj.jwzp.hellodi.logic.MovieLister;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class SpringMain {

    public static void main(String[] args) throws IOException {

        FileName file = new FileName(args);
        String fileName = file.getFileName();
        String fileFormat = file.getFileFormat();

        final ApplicationContext ctx = new AnnotationConfigApplicationContext(
            "uj.jwzp.hellodi.logic", "uj.jwzp.hellodi.logic.savers");

        FileName fileBean = ctx.getBean(FileName.class);
        fileBean.setFileName(fileName);
        fileBean.setFileFormat(fileFormat);

        MovieSaver saver = (MovieSaver) ctx.getBean(fileFormat, MovieSaver.class);


        MovieLister lister = (MovieLister) ctx.getBean("movieLister");
        List<Movie> movies = lister.moviesDirectedBy("Lucas").stream()
            .peek(m -> System.out.println(m.toString()))
            .collect(Collectors.toList());
        saver.save(movies);
    }

}
