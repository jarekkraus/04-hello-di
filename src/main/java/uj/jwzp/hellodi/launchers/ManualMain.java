package uj.jwzp.hellodi.launchers;

import uj.jwzp.hellodi.logic.BadFileFormatException;
import uj.jwzp.hellodi.logic.savers.*;
import uj.jwzp.hellodi.model.Movie;
import uj.jwzp.hellodi.logic.CSVMovieFinder;
import uj.jwzp.hellodi.logic.MovieFinder;
import uj.jwzp.hellodi.logic.MovieLister;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ManualMain {

    public static void main(String[] args) throws IOException {
        MovieFinder finder = new CSVMovieFinder();
        MovieLister lister = new MovieLister(finder);
        MovieSaver saver;

        FileName file = new FileName(args);
        String fileFormat = file.getFileFormat();

        switch (FileType.valueOf(fileFormat.toUpperCase())) {
            case XML:
                saver = new XmlMovieSaver(file);
                break;
            case JSON:
                saver = new JsonMovieSaver(file);
                break;
            case YML:
                saver = new YmlMovieSaver(file);
                break;
            default:
                throw new BadFileFormatException();
        }

        List<Movie> movies = lister.moviesDirectedBy("Lucas").stream()
                .peek(m -> System.out.println(m.toString()))
                .collect(Collectors.toList());

        saver.save(movies);
    }

}
