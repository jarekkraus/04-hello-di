package uj.jwzp.hellodi.launchers;

import dagger.Module;
import dagger.Provides;
import uj.jwzp.hellodi.logic.savers.*;
import uj.jwzp.hellodi.logic.CSVMovieFinder;
import uj.jwzp.hellodi.logic.MovieFinder;
import uj.jwzp.hellodi.logic.MovieLister;

import javax.inject.Singleton;

@Module
public class MovieModule {

    @Provides
    @Singleton
    public FileName provideFileName() {
        return new FileName();
    }

    @Provides
    @Singleton
    public MovieFinder provideMovieFinder() {
        return new CSVMovieFinder();
    }

    @Provides
    @Singleton
    public MovieLister provideMovieLister(MovieFinder movieFinder) {
        return new MovieLister(movieFinder);
    }

    @Provides
    @Singleton
    public MovieSaver provideMovieSaver(FileName file) {
        switch (FileType.valueOf(file.getFileFormat().toUpperCase())) {
            case JSON:
                return new JsonMovieSaver(file);
            case YML:
                return new YmlMovieSaver(file);
            default:
                return new XmlMovieSaver(file);
        }
    }

}
