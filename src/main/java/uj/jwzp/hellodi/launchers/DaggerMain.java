package uj.jwzp.hellodi.launchers;

import uj.jwzp.hellodi.logic.savers.FileName;
import uj.jwzp.hellodi.logic.savers.MovieSaver;
import uj.jwzp.hellodi.model.Movie;
import uj.jwzp.hellodi.logic.MovieLister;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class DaggerMain {

    public static void main(String[] args) throws IOException {

        FileName file = new FileName(args);
        String fileName = file.getFileName();
        String fileFormat = file.getFileFormat();

        DaggerComponent daggerComponent = DaggerDaggerComponent.builder()
                .movieModule(new MovieModule())
                .build();

        FileName fileDagger = daggerComponent.getFileName();
        fileDagger.setFileName(fileName);
        fileDagger.setFileFormat(fileFormat);

        MovieLister lister = daggerComponent.getMovieLister();
        MovieSaver saver = daggerComponent.getMovieSaver();

        List<Movie> movies = lister.moviesDirectedBy("Lucas").stream()
                .peek(m -> System.out.println(m.toString()))
                .collect(Collectors.toList());
        saver.save(movies);
    }
}
