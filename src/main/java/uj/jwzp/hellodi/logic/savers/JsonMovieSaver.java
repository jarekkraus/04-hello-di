package uj.jwzp.hellodi.logic.savers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uj.jwzp.hellodi.model.Movie;
import uj.jwzp.hellodi.model.MovieSerializer;

import javax.inject.Inject;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

@Component("json")
public class JsonMovieSaver implements MovieSaver {

    @Autowired
    private FileName fileName;

    public void setFileName(FileName fileName) {
        this.fileName = fileName;
    }

    public JsonMovieSaver(){ }

    @Inject
    public JsonMovieSaver(FileName fileName) {
        this.fileName = fileName;
    }

    @Override
    public void save(List<Movie> movies) throws IOException {
        Writer writer = new FileWriter(fileName.getFullFileName());

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Movie.class, new MovieSerializer())
                .setPrettyPrinting()
                .create();
        gson.toJson(movies, writer);
        writer.append('\n');
        writer.close();
    }
}
