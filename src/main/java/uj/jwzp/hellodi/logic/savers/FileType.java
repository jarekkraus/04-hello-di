package uj.jwzp.hellodi.logic.savers;

public enum FileType {
    XML, JSON, YML
}
