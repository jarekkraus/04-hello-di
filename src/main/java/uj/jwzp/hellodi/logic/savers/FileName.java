package uj.jwzp.hellodi.logic.savers;

import org.springframework.stereotype.Component;
import uj.jwzp.hellodi.logic.BadFileFormatException;

import javax.inject.Singleton;

@Component
@Singleton
public class FileName {
    private String fileName = "saved";
    private String fileFormat = "xml";

    public FileName() {}

    public FileName(String[] args) throws BadFileFormatException {
        this.fileName = getFileNameFromArgs(args);
        String fileFormat = getFileFormatFromArgs(args);
        if (checkFileFormat(fileFormat)) {
            this.fileFormat = fileFormat;
        } else {
            throw new BadFileFormatException();
        }
    }

    public String getFullFileName() {
        return fileName + "." + fileFormat;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    public String getFileFormat() {
        return fileFormat;
    }

    private String getFileFormatFromArgs(String[] args) {
        if (args.length > 1) {
            return args[1];
        }
        return "xml";
    }

    private String getFileNameFromArgs(String[] args) {
        if (args.length > 0) {
            return args[0].split("\\.")[0];
        }
        return "saved";
    }

    private boolean checkFileFormat(String fileFormat) {
        switch (FileType.valueOf(fileFormat.toUpperCase())) {
            case XML:
            case JSON:
            case YML:
                return true;
        }
        return false;
    }
}
