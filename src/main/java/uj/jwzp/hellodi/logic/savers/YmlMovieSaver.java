package uj.jwzp.hellodi.logic.savers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uj.jwzp.hellodi.model.Movie;

import javax.inject.Inject;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

@Component("yml")
public class YmlMovieSaver implements MovieSaver {

    @Autowired
    private FileName fileName;

    public void setFileName(FileName fileName) {
        this.fileName = fileName;
    }

    public YmlMovieSaver(){ }

    @Inject
    public YmlMovieSaver(FileName fileName) {
        this.fileName = fileName;
    }

    @Override
    public void save(List<Movie> movies) throws IOException {
        Writer writer = new FileWriter(fileName.getFullFileName());
        writer.append("movies:\n");
        for (Movie movie: movies) {
            writer.append(movieEntry(movie));
        }
        writer.close();
    }

    private String movieEntry(Movie movie) {
        StringBuilder result = new StringBuilder("  -\n");
        result
                .append("    title: \"" + movie.getTitle() + "\"\n")
                .append("    director: " + movie.getDirector() + "\n")
                .append("    year: " + movie.getYear() + "\n");
        return result.toString();
    }
}
