package uj.jwzp.hellodi.logic.savers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uj.jwzp.hellodi.model.Movie;

import javax.inject.Inject;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

@Component("xml")
public class XmlMovieSaver implements MovieSaver {

    @Autowired
    private FileName fileName;

    public void setFileName(FileName fileName) {
        this.fileName = fileName;
    }

    public XmlMovieSaver(){ }

    @Inject
    @Autowired
    public XmlMovieSaver(FileName fileName) {
        this.fileName = fileName;
    }

    @Override
    public void save(List<Movie> movies) throws IOException {
        Writer writer = new FileWriter(fileName.getFullFileName());
        writer.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n").append("<movies>\n");
        for (Movie movie: movies) {
            writer.append(movieEntry(movie));
        }
        writer.append("</movies>\n");
        writer.close();
    }

    private String movieEntry(Movie movie) {
        StringBuilder result = new StringBuilder("  <movie>\n");
        result
            .append("    <title>" + movie.getTitle() + "</title>\n")
            .append("    <director>" + movie.getDirector() + "</director>\n")
            .append("    <year>" + movie.getYear() + "</year>\n")
            .append("  </movie>\n");
        return result.toString();
    }
}
