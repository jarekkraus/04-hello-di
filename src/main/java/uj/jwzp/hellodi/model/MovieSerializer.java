package uj.jwzp.hellodi.model;

import com.google.gson.*;

import java.lang.reflect.Type;

public class MovieSerializer implements JsonSerializer<Movie> {
    @Override
    public JsonElement serialize(Movie src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject result = new JsonObject();
        result.add("title", new JsonPrimitive(src.getTitle()));
        result.add("director", new JsonPrimitive(src.getDirector()));
        result.add("year", new JsonPrimitive(Integer.toString(src.getYear())));

        return result;
    }
}
